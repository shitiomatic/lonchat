package main

import (
	"context"
	"flag"
	"log"

	"bitbucket.org/shitiomatic/lonchat"
)

func main() {
	var (
		signalerURL = flag.String("signaler-url", "wss://sh.lonchat.shitiomatic.tech/ws", "host and port of the signaler")
	)
	flag.Parse()

	ctx := context.Background()

	app, err := lonchat.New(*signalerURL)
	if err != nil {
		log.Fatal(err)
	}

	if err := app.Run(ctx); err != nil {
		log.Fatal(err)
	}
}
