#!/bin/bash

#  LonChat v1.0.0
#
#  presented by:
#
#        _         _                                _             
#       ( )     _ ( )_  _                          ( )_  _        
#   ___ | |__  (_)| ,_)(_)   _     ___ ___     _ _ | ,_)(_)   ___ 
# /',__)|  _ `\| || |  | | /'_`\ /' _ ` _ `\ /'_` )| |  | | /'___)
# \__, \| | | || || |_ | |( (_) )| ( ) ( ) |( (_| || |_ | |( (___ 
# (____/(_) (_)(_)`\__)(_)`\___/'(_) (_) (_)`\__,_)`\__)(_)`\____)
#
#
# LonChat is a command-line based video chat client that connects
# you with a random person. It was written as a demonstration of the
# Pion WebRTC library.
#
# [[ This project is completely open source! ]]
# Bitbucket: https://bitbucket.org/shitiomatic/lonchat
# Website: https://lonchat.shitiomatic.tech
# Email: human@shitiomatic.email

function fail_unsupported() {
    platform="${1:-"your platform"}"
    echo "LonChat isn't supported on $platform yet :-( Try Mac or Linux instead."
    echo ""
    echo "Contributions are welcome! https://bitbucket.org/shitiomatic/lonchat"
    exit 1
}

function detect_platform() {
    case $(uname | tr '[:upper:]' '[:lower:]') in
        linux*)
            case $(uname -m) in
                x86_64)
                    echo "linux64"
                ;;
                *)
                    fail_unsupported "32-bit Linux"
                ;;
            esac
        ;;
        darwin*)
            case $(uname -m) in
                x86_64)
                    echo "darwin64"
                ;;
                *)
                    fail_unsupported "32-bit Macs"
                ;;
            esac
        ;;
        msys*)
            fail_unsupported "Windows"
        ;;
        *)
            fail_unsupported
        ;;
    esac
}

function getMD5() {
    file="$1"
    if ! [ -f "$file" ]; then
        return
    fi
    
    if [ -x "$(command -v md5sum)" ]; then
        md5sum | cut -d ' ' -f 1
        elif [ -x "$(command -v md5)" ]; then
        md5 -r $file | cut -d ' ' -f 1
    fi
}

function checkEtag() {
    url="$1"
    etag="$2"
    
    code="$(curl -s --head -o /dev/null -w "%{http_code}" -H "If-None-Match: $etag" "$url")"
    
    if [ "$code" != "304" ]; then
        echo "changed"
    fi
}

function main() {
    platform="$(detect_platform)"
    binaryURL="https://lonchat.shitiomatic.tech/lonchat.$platform"
    md5="$(getMD5 /tmp/lonchat)"
    
    if [ -n "$(checkEtag $binaryURL $md5)" ]; then
        printf "Downloading LonChat\033[5m...\033[0m\n"
        curl -fs "$binaryURL" > /tmp/lonchat
    fi
    
    chmod +x /tmp/lonchat
    
    clear
    /tmp/lonchat
    
    if [ $? -eq 0 ]; then
        reset
    fi
}

echo <<EOF
